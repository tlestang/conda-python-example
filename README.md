# conda-python-repro-test

An experiment to monitor reproducibility of a simple conda environment.

This repo contains a conda environment file with a simple python
script using numpy, scipy and matplotlib.

This repo defines four different pipelines, defined in four different
branches:

- `miniconda-latest`: Create environment within the `miniconda3:latest` image.
- `miniconda3-4.12`: Create environment within the `miniconda3:4.12` image.
- `debian`: Install latest miniconda3 and create environment in `debian:latest` image.
- `ubuntu`: Install latest miniconda3 and create environment in `ubuntu:latest` image.

After creating the environment, all four pipelines run `src.py`.

The scheduled pipelines runs every week.
