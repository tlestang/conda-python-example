import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

def model(x, p1, p2):
    """A mathematical model we'd like to fit to some noisy data. Has
    two real valued parameters p1 and p2.
    """
    return p1 * np.cos(p2 * x) + p2 * np.sin(p1 * x)

# Get an instance of np.random.Generator to generate random numbers
rng = np.random.default_rng()

# Generate fake noisy data from the model itself
x = np.linspace(0, 2 * 3.14, 100)
noise_amplitude = 0.3
noisy_data = model(x, p1=1, p2=0.75) + rng.random(len(x)) * noise_amplitude

initial_guess = (3, 0.6)
popt, _ = curve_fit(model, x, noisy_data, p0=initial_guess)

fig, ax = plt.subplots()
ax.plot(x, noisy_data, 'bo')
ax.plot(x, model(x, *popt), '--r')
ax.set_title(f"p1 = {popt[0]}, p2 = {popt[1]}")
plt.savefig("figure.svg")
